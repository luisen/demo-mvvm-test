function updateUi (state) {
    updateMessage(state.message)
}

function updateMessage (message) {
    document.getElementById('message').innerHTML = message
}

try {
    module.exports = { updateMessage }
} catch (error) {

}