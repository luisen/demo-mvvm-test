const { updateMessage } = require('../ui.js')

test('updateMessage', function () {
    document.body.innerHTML = `
        <div id="message"></div>
    `
    updateMessage('aupa')

    expect(document.getElementById('message').innerHTML).toBe('aupa')

})